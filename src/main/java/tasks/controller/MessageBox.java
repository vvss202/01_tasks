package tasks.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
public class MessageBox {
    public static void showErrorMessage(String header, String message){
        showAlert(AlertType.ERROR, "Error", header, message);
    }

    private static void showAlert(AlertType type, String title,String header, String message){
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
