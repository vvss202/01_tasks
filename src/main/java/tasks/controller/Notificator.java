package tasks.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import org.controlsfx.control.Notifications;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.TaskList;
import tasks.services.TaskIO;
import tasks.view.Main;

import java.io.IOException;
import java.util.Date;

public class Notificator extends Thread {

    private static final int millisecondsInSec = 1000;
    private static final int secondsInMin = 60;

    private static final Logger log = Logger.getLogger(Notificator.class.getName());
    private static Stage primaryStage;

    public Notificator(Stage stage){
        Notificator.primaryStage=stage;
    }

    @Override
    public void run() {
        Date currentDate = new Date();
        while (true) {
            TaskList tasks = new ArrayTaskList();
            log.info("Notificator started new verification");
            try {
                TaskIO.readBinary(tasks, Main.savedTasksFile);
            } catch (IOException ex){
                log.error("Error reading tasks from file");
            }
            for (Task t : tasks) {
                if (t.isActive()) {
                    if (t.isRepeated() && t.getEndTime().after(currentDate)){

                        Date next = t.nextTimeAfter(currentDate);
                        if (null != next){
                            long currentMinute = getTimeInMinutes(currentDate);
                            long taskMinute = getTimeInMinutes(next);
                            if (taskMinute-t.getRepeatInterval()/secondsInMin == currentMinute){
                                showNotification(t);
                            }
                        }
                    }
                    else {
                        if (!t.isRepeated()){
                            if (getTimeInMinutes(currentDate) == getTimeInMinutes(t.getStartTime())){
                                showNotification(t);
                            }
                        }
                    }
                }

            }
            try {
                Thread.sleep(millisecondsInSec*secondsInMin);

            } catch (InterruptedException e) {
                log.error("thread interrupted exception");
            }
            currentDate = new Date();
        }
    }
    public static void showNotification(Task task){
        log.info("push notification showing");
        Platform.runLater(() -> {
            Notifications
                    .create()
                    .title("Task reminder")
                    .text("It's time for " + task.getTitle())
                    .owner(primaryStage)
                    .showInformation();
        });
    }
    private static long getTimeInMinutes(Date date){
        return date.getTime()/(millisecondsInSec*secondsInMin);
    }
}