package tasks.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import tasks.model.Task;

import org.apache.log4j.Logger;

public class TaskInfoController {

    private static final Logger log = Logger.getLogger(TaskInfoController.class.getName());
    @FXML
    private Label labelTitle;
    @FXML
    private Label labelStart;
    @FXML
    private Label labelEnd;
    @FXML
    private Label labelInterval;
    @FXML
    private Label labelIsActive;

    @FXML
    public void initialize(){
        log.info("task info window initializing");
    }

    @FXML
    public void closeWindow(){
        Controller.infoStage.close();
    }

    public void initializeTask(Task task) {
        labelTitle.setText("Title: " + task.getTitle());
        labelStart.setText("Start time: " + task.getFormattedDateStart());
        labelEnd.setText("End time: " + task.getFormattedDateEnd());
        labelInterval.setText("Interval: " + task.getFormattedRepeated());
        labelIsActive.setText("Is active: " + (task.isActive() ? "Yes" : "No"));
    }
}
