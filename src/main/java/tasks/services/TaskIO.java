package tasks.services;

import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import tasks.model.LinkedTaskList;
import tasks.model.Task;
import tasks.model.TaskList;
import tasks.view.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TaskIO {
    private static final String[] TIME_ENTITY = {" day"," hour", " minute"," second"};
    private static final int secondsInDay = 86400;
    private static final int secondsInHour = 3600;
    private static final int secondsInMin = 60;

    private static final Logger log = Logger.getLogger(TaskIO.class.getName());
    public static void write(TaskList tasks, OutputStream out) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(out);
        try {
            dataOutputStream.writeInt(tasks.size());
            for (Task t : tasks){
                dataOutputStream.writeInt(t.getTitle().length());
                dataOutputStream.writeUTF(t.getTitle());
                dataOutputStream.writeBoolean(t.isActive());
                dataOutputStream.writeInt(t.getRepeatInterval());
                if (t.isRepeated()){
                    dataOutputStream.writeLong(t.getStartTime().getTime());
                    dataOutputStream.writeLong(t.getEndTime().getTime());
                }
                else {
                    dataOutputStream.writeLong(t.getStartTime().getTime());
                }
            }
        }
        finally {
            dataOutputStream.close();
        }
    }

    public static void read(TaskList tasks, InputStream in)throws IOException {
        DataInputStream dataInputStream = new DataInputStream(in);
        try {
            int listLength = dataInputStream.readInt();
            for (int i = 0; i < listLength; i++){
                int titleLength = dataInputStream.readInt();
                String title = dataInputStream.readUTF();
                boolean isActive = dataInputStream.readBoolean();
                int interval = dataInputStream.readInt();
                Date startTime = new Date(dataInputStream.readLong());
                Task taskToAdd;
                try {
                    if (interval > 0){
                        Date endTime = new Date(dataInputStream.readLong());
                        taskToAdd = new Task(title, startTime, endTime, interval);
                    }
                    else {
                        taskToAdd = new Task(title, startTime);
                    }
                }catch (IllegalArgumentException ex){
                    continue;
                }
                taskToAdd.setActive(isActive);
                tasks.add(taskToAdd);
            }
        }
        finally {
            dataInputStream.close();
        }
    }

    public static void writeBinary(TaskList tasks, File file)throws IOException{
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            write(tasks,fos);
        }
        catch (IOException e){
            log.error("IO exception reading or writing file");
        }
        finally {
            fos.close();
        }
    }

    public static void readBinary(TaskList tasks, File file) throws IOException{
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            read(tasks, fis);
        }
        catch (IOException e){
            log.error("IO exception reading or writing file");
        }
        finally {
            fis.close();
        }
    }

    public static String getFormattedInterval(int interval){
        if (interval <= 0) throw new IllegalArgumentException("Interval <= 0");
        StringBuilder sb = new StringBuilder();

        int days = interval/secondsInDay;
        int hours = (interval - secondsInDay*days) / secondsInHour;
        int minutes = (interval - (secondsInDay*days + secondsInHour*hours)) / secondsInMin;
        int seconds = (interval - (secondsInDay*days + secondsInHour*hours + secondsInMin*minutes));

        int[] time = new int[]{days, hours, minutes, seconds};
        int i = 0, j = time.length-1;
        while (time[i] == 0 || time[j] == 0){
            if (time[i] == 0) i++;
            if (time[j] == 0) j--;
        }

        for (int k = i; k <= j; k++){
            sb.append(time[k]);
            sb.append(time[k] > 1 ? TIME_ENTITY[k]+ "s" : TIME_ENTITY[k]);
            sb.append(" ");
        }
        return sb.toString();
    }


    public static void rewriteFile(ObservableList<Task> tasksList, File tasksFile) {
        LinkedTaskList taskList = new LinkedTaskList();
        for (Task t : tasksList){
            taskList.add(t);
        }
        try {
            TaskIO.writeBinary(taskList, tasksFile);
        }
        catch (IOException e){
            log.error("IO exception reading or writing file");
        }
    }
}
