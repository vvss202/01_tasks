package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.validators.TaskValidator;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class TasksService {

    private ArrayTaskList tasks;
    private TaskValidator taskValidator;
    private static final Logger log = Logger.getLogger(TasksService.class.getName());

    public TasksService(ArrayTaskList tasks, TaskValidator taskValidator){
        this.tasks = tasks;
        this.taskValidator = taskValidator;
    }


    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }

    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }

    public String formTimeUnit(int timeUnit){
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }

    public int parseFromStringToSeconds(String stringTime){//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);
        int result = (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;
        return result;
    }

    public Iterable<Task> filterTasks(ObservableList<Task> tasks,  Date start, Date end){
        log.info("TaskService: filterTasks() called with parameters tasks=" + tasks + "start=" +start+" end="+end);
        ArrayList<Task> incomingTasks = new ArrayList<>();
        if (!end.before(start)){
            for (int i=0; i<tasks.size(); i++) {
                Task currentTask = tasks.get(i);
                Date nextTime = currentTask.nextTimeAfter(start);
                if (nextTime != null) {
                    if (nextTime.before(end) || nextTime.equals(end)) {
                        incomingTasks.add(currentTask);
                        log.info(currentTask.getTitle() + "with start=" + currentTask.getFormattedDateStart() + "selected");
                    }
                }
            }
        }
        return incomingTasks;
    }

    /**
     * Metoda care valideaza un task, iar daca ecesta este valid il adauga in fisier
     * @param task - task-ul ce se doreste a fi adaugat in fisier
     * @param tasks - lista initiala de task-uri in care se va adauga acesta
     * @param tasksFile - fisierul in care se va scrie lista de task-uri
     * @return task-ul, daca este adaugat cu succes
     * @throws IllegalArgumentException daca task-ul nu este valid
     * pentru un task valid {
     *     titlul trebuie sa nu fie null sau string gol
     *     startDate trebuie sa fie mai mare decat 1970 si mai mica decat endDate in cazul in care task-ul este repetitiv
     *     endDate trebuie sa fie mai mare decat 1970
     *     interval trebuie sa fie mai mare decat 1 in cazul in care task-ul este repetitiv
     * }
     */
    public Task addTask(Task task, ObservableList<Task> tasks, File tasksFile) throws IllegalArgumentException{
        taskValidator.validate(task);
        tasks.add(task);
        TaskIO.rewriteFile(tasks,tasksFile);
        return task;
    }
}
