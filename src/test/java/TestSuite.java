import org.junit.platform.suite.api.*;

@Suite
@SelectPackages({"tasks.services"})
@SuiteDisplayName("Suite for service tests")
public class TestSuite {
}
