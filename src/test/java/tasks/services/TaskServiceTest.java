package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import tasks.model.ArrayTaskList;
import tasks.model.LinkedTaskList;
import tasks.model.Task;
import tasks.model.TaskList;
import tasks.validators.TaskValidator;
import tasks.view.Main;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class TaskServiceTest {
    private static File testFile;
    private TasksService service;
    private ObservableList<Task> observableList;
    private ArrayTaskList taskList;

    @BeforeEach
    void setUp() {
        testFile = new File(Main.class.getClassLoader().getResource("data/test_tasks.txt").getFile());
        taskList = new ArrayTaskList();
        observableList = FXCollections.observableArrayList(taskList.getAll());
        service = new TasksService(taskList, new TaskValidator());
    }

    @AfterEach
    void tearDown() throws IOException {
        TaskList tasks = new LinkedTaskList();
        TaskIO.writeBinary(tasks, testFile);
    }

    @Test
    void getLocalDateValueFromDate() {
    }

    @Test
    void getDateValueFromLocalDate() {
    }

    @Test
    @DisplayName("Test for adding a valid task ECP")
    void testAddTaskValidECP() throws ParseException, IOException {
        Task t = new Task("This is a valid task", Task.getDateFormat().parse("2021-01-17 10:00"),
                Task.getDateFormat().parse("2021-01-18 12:00"), 30);
        Assertions.assertEquals(service.addTask(t, observableList, testFile), t);
        Assertions.assertEquals(observableList.size(), 1);
        TaskIO.readBinary(taskList, testFile);
        Assertions.assertEquals(taskList.size(), 1);
    }

    @Test
    @DisplayName("Test for adding an invalid task ECP")
    void testAddTaskInvalidECP() throws IOException, ParseException {
        Task invalidTask = new Task("", Task.getDateFormat().parse("2021-01-17 10:00"),
                Task.getDateFormat().parse("1969-01-18 12:00"), 30);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> service.addTask(invalidTask, observableList, testFile));
        Assertions.assertEquals("Start date should be before end", exception.getMessage());
        Assertions.assertEquals(observableList.size(), 0);
        TaskIO.readBinary(taskList, testFile);
        Assertions.assertEquals(taskList.size(), 0);
    }

    @ParameterizedTest
    @MethodSource("generatorECP")
    @DisplayName("Test for adding an invalid task ECP")
    void testAddTaskInvalidECPWithMethodSource(Task task, String exceptionMessage ) throws IOException {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> service.addTask(task, observableList, testFile));
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
        Assertions.assertEquals(observableList.size(), 0);
        TaskIO.readBinary(taskList, testFile);
        Assertions.assertEquals(taskList.size(), 0);
    }

    static Stream<Arguments> generatorECP() throws ParseException {
        return Stream.of(
                Arguments.of(new Task(null, Task.getDateFormat().parse("2021-01-17 10:00"),
                        Task.getDateFormat().parse("2021-01-18 12:00"), 30), "Invalid title!"),
                Arguments.of(new Task("", Task.getDateFormat().parse("2021-01-17 10:00"),
                        Task.getDateFormat().parse("2021-01-18 12:00"), 30), "Invalid title!"),
                Arguments.of(new Task("", Task.getDateFormat().parse("2021-01-17 10:00"),
                        Task.getDateFormat().parse("2021-01-10 12:00"), 30), "Start date should be before end")
        );
    }

    @Test
    @DisplayName("Test for adding a valid task BVA")
    void testAddTaskValidBVA() throws ParseException, IOException {
        Task t1 = new Task("T", Task.getDateFormat().parse("2021-01-17 10:00"),
                Task.getDateFormat().parse("2021-01-18 12:00"), 30);
        Task t2 = new Task("This is a valid task", Task.getDateFormat().parse("1971-01-01 10:00"),
                Task.getDateFormat().parse("1971-01-02 12:00"), 30);
        Assertions.assertEquals(service.addTask(t1, observableList, testFile), t1);
        Assertions.assertEquals(service.addTask(t2, observableList, testFile), t2);
        Assertions.assertEquals(observableList.size(), 2);
        TaskIO.readBinary(taskList, testFile);
        Assertions.assertEquals(taskList.size(), 2);
    }

    @Test
    @DisplayName("Test for adding an invalid task BVA")
    void testAddTaskInvalidBVA() throws IOException, ParseException {
        Task invalidTask = new Task("This is a valid task", Task.getDateFormat().parse("1969-01-01 10:00"),
                Task.getDateFormat().parse("1969-01-02 12:00"), 30);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> service.addTask(invalidTask, observableList, testFile));
        Assertions.assertEquals("Invalid date!", exception.getMessage());
        Assertions.assertEquals(observableList.size(), 0);
        TaskIO.readBinary(taskList, testFile);
        Assertions.assertEquals(taskList.size(), 0);
    }

    @ParameterizedTest
    @MethodSource("generatorBVA")
    @DisplayName("Test for adding an invalid task BVA")
    void testAddTaskInvalidBVAWithMethodSource(Task task, String exceptionMessage ) throws IOException {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> service.addTask(task, observableList, testFile));
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
        Assertions.assertEquals(observableList.size(), 0);
        TaskIO.readBinary(taskList, testFile);
        Assertions.assertEquals(taskList.size(), 0);
    }

    private static Stream<Arguments> generatorBVA() throws ParseException {
        return Stream.of(
                Arguments.of(new Task(null, Task.getDateFormat().parse("2021-01-01 10:00"),
                        Task.getDateFormat().parse("2021-02-01 12:00"), 30), "Invalid title!"),
                Arguments.of(new Task("This is a valid task", Task.getDateFormat().parse("1969-01-01 10:00"),
                        Task.getDateFormat().parse("1969-01-02 12:00"), 30), "Invalid date!")
        );
    }

    @Test
    @DisplayName("F02_TC01")
    void testFilterTasksInvalidDates() throws ParseException {
        Task t1 = new Task("T", Task.getDateFormat().parse("2022-04-02 12:00"),
                Task.getDateFormat().parse("2022-04-05 12:00"), 30);
        observableList.add(t1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start = formatter.parse("2022-04-02 12:00");
        Date end = formatter.parse("2022-04-01 12:00");
        Iterable<Task> tasks = service.filterTasks(observableList, start, end);
        List<Task> filtered = StreamSupport.stream(tasks.spliterator(),false).collect(Collectors.toList());
        Assertions.assertEquals(filtered.size(),0);
    }

    @Test
    @DisplayName("F02_TC02")
    void testFilterTasksEmptyList() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start = formatter.parse("2022-04-02 12:00");
        Date end = formatter.parse("2022-04-01 12:00");
        Iterable<Task> tasks = service.filterTasks(observableList, start, end);
        List<Task> filtered = StreamSupport.stream(tasks.spliterator(),false).collect(Collectors.toList());
        Assertions.assertEquals(filtered.size(),0);
    }

    @Test
    @DisplayName("F02_TC03")
    void testFilterTasksNonRepetitiveTask() throws ParseException{
        Task t1 = new Task("T", Task.getDateFormat().parse("2022-04-03 12:00"));
        observableList.add(t1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start = formatter.parse("2022-04-02 12:00");
        Date end = formatter.parse("2022-04-03 12:00");
        Iterable<Task> tasks = service.filterTasks(observableList, start, end);
        List<Task> filtered = StreamSupport.stream(tasks.spliterator(),false).collect(Collectors.toList());
        Assertions.assertEquals(filtered.size(),0);
    }

    @Test
    @DisplayName("F02_TC04")
    void testFilterTasksStartDateNotInInterval() throws ParseException{
        Task t1 = new Task("T", Task.getDateFormat().parse("2022-04-04 12:00"),
                Task.getDateFormat().parse("2022-04-05 12:00"), 30);
        t1.setActive(true);
        observableList.add(t1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start = formatter.parse("2022-04-02 12:00");
        Date end = formatter.parse("2022-04-03 12:00");
        Iterable<Task> tasks = service.filterTasks(observableList, start, end);
        List<Task> filtered = StreamSupport.stream(tasks.spliterator(),false).collect(Collectors.toList());
        Assertions.assertEquals(filtered.size(),0);
    }

    @Test
    @DisplayName("F02_TC05")
    void filterTasksStartDateInInterval() throws ParseException {
        Task t1 = new Task("T", Task.getDateFormat().parse("2022-04-04 12:00"),
                Task.getDateFormat().parse("2022-04-05 12:00"), 30);
        t1.setActive(true);
        observableList.add(t1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start = formatter.parse("2022-04-02 12:00");
        Date end = formatter.parse("2022-04-04 12:00");
        Iterable<Task> tasks = service.filterTasks(observableList, start, end);
        List<Task> filtered = StreamSupport.stream(tasks.spliterator(),false).collect(Collectors.toList());
        Assertions.assertEquals(filtered.size(),1);
        Assertions.assertEquals(filtered.get(0),t1);
    }

    @Test
    @DisplayName("F02_TC06")
    void filterTasksNotValid() throws ParseException{
        Task t1 = new Task("T", Task.getDateFormat().parse("2022-04-03 12:00"),
                Task.getDateFormat().parse("2022-04-06 12:00"), 30);
        t1.setActive(true);
        observableList.add(t1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start = formatter.parse("2022-04-02 12:00");
        Date end = formatter.parse("2022-04-04 12:30");
        Iterable<Task> tasks = service.filterTasks(observableList, start, end);
        List<Task> filtered = StreamSupport.stream(tasks.spliterator(),false).collect(Collectors.toList());
        Assertions.assertEquals(filtered.size(),1);
        Assertions.assertEquals(filtered.get(0),t1);
    }
}
